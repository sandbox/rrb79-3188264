<?php

namespace Drupal\helpdesk_uvdesk\Plugin\HelpdeskIntegration;

use Drupal\comment\CommentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\PluginBase;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the UVDesk helpdesk.
 *
 * @HelpdeskPlugin(
 *   id = "uvdesk",
 *   label = @Translation("Uvdesk"),
 *   description = @Translation("Provides the Uvdesk helpdesk plugin.")
 * )
 */
class Uvdesk extends PluginBase {

  use StringTranslationTrait;

  /**
   * GuzzleHttp\Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    $instance = parent::create($container, $configuration, $pluginId, $pluginDefinition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function settingKeys(): array {
    return [
      'url',
      'api_token',
      'group',
      'max_file_size',
      'states',
      'state_closed',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(HelpdeskInterface $helpdesk): array {

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $helpdesk->get('url'),
      '#required' => TRUE,
    ];
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $helpdesk->get('api_token'),
      '#required' => TRUE,
    ];
    $form['group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group'),
      '#default_value' => $helpdesk->get('group'),
    ];
    $form['max_file_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. file size for attachments (in MB)'),
      '#default_value' => $helpdesk->get('max_file_size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $states = $helpdesk->get('url') ? $this->getStates($helpdesk) : [];
    $form['states'] = [
      '#type' => 'value',
      '#value' => $states,
    ];
    $form['state_closed'] = [
      '#type' => 'select',
      '#title' => $this->t('Closed ticket state'),
      '#default_value' => $helpdesk->get('state_closed'),
      '#options' => $states,
    ];

    return $form;
  }

  /**
   * Get all states from the helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   *
   * @return array
   *   List of all active states indexed by their remote state ID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function getStates(HelpdeskInterface $helpdesk) {
    $uri = $helpdesk->get('url') . 'tickets';
    $token = $helpdesk->get('api_token');

    $contents = $this->request('GET', $uri, $token);
    $statuses = $contents->status ?? [];

    $states = [];

    try {
      foreach ($statuses as $status) {
        $states[$status->id] = $status->description;
      }
    }
    catch (HelpdeskPluginException $e) {
      // Ignore this exception as we may call this while not configured yet.
    }
    return $states;
  }

  /**
   * Search for the Drupal user by its remote id.
   *
   * @return \Drupal\user\UserInterface
   *   The Drupal user entity.
   */
  private function findUserByRemoteId(): UserInterface {
    // @todo Create better implementation.
    return User::load(1);
  }

  /**
   * {@inheritdoc}
   */
  public function pushUser(HelpdeskInterface $helpdesk, UserInterface $user): void {
    // Required by interface, but not supported by Uvdesk.
  }

  /**
   * {@inheritdoc}
   */
  public function createIssue(HelpdeskInterface $helpdesk, IssueInterface $issue) {
    // @todo Create implementation.
  }

  /**
   * {@inheritdoc}
   */
  public function addCommentToIssue(HelpdeskInterface $helpdesk, IssueInterface $issue, CommentInterface $comment) {
    // @todo Create implementation.
  }

  /**
   * {@inheritdoc}
   */
  public function resolveIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    // @todo Create implementation.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAllIssues(HelpdeskInterface $helpdesk, UserInterface $user, int $since = 0): array {
    $issues = [];

    $uri = $helpdesk->get('url') . 'tickets';
    $token = $helpdesk->get('api_token');

    $contents = $this->request('GET', $uri, $token);
    $tickets = $contents->tickets ?? [];

    foreach ($tickets as $ticket) {
      $ticketUri = $helpdesk->get('url') . 'ticket/' . $ticket->id;
      $response = $this->request('GET', $ticketUri, $token);
      $issues[] = $this->saveIssue($helpdesk, $response->ticket);
    }
    return $issues;
  }

  /**
   * Save local issue from given data.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   Related helpdesk.
   * @param object $ticket
   *   Ticket data.
   *
   * @return \Drupal\helpdesk_integration\Entity\Issue
   *   Issue entity.
   */
  private function saveIssue(HelpdeskInterface $helpdesk, object $ticket):IssueInterface {
    /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
    $issue = Issue::create([
      'helpdesk' => $helpdesk->id(),
      'extid' => $ticket->id,
      'resolved' => (string) $ticket->status->code === 'closed',
      'title' => $ticket->subject,
      'status' => $ticket->status->description,
      'body' => [
        'value' => $ticket->threads[0]->message ?? '',
        'format' => 'basic_html',
      ],
      'created' => $ticket->createdAt->timestamp,
      'changed' => $ticket->updatedAt->timestamp,
    ]);
    foreach ($ticket->threads as $key => $thread) {
      if ($key === 0) {
        continue;
      }
      $issue->addComment(
        $thread->id,
        $thread->message,
        $this->findUserByRemoteId(),
        $thread->createdAt->timestamp,
        $thread->updatedAt->timestamp
      );
    }
    return $issue;
  }

  /**
   * Send http request to Uvdesk api by given parameters.
   *
   * @param string $method
   *   Method: GET|POST|PUT|PATCH|DELETE.
   * @param string $uri
   *   Absolute uri.
   * @param string $token
   *   Uvdesk api token.
   *
   * @return object
   *   Decoded api response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request(string $method, string $uri, string $token):object {
    $contents = $this->httpClient->request($method, $uri, [
      'headers' => ['Authorization' => 'Basic ' . $token],
    ])->getBody()->getContents();
    return json_decode($contents);
  }

}
