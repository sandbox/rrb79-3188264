<?php

namespace Drupal\helpdesk_uvdesk;

use Drupal\helpdesk_integration\Service as BaseService;
use Drupal\helpdesk_uvdesk\Plugin\HelpdeskIntegration\Uvdesk;

/**
 * Helpdesk Services.
 */
class Service {

  /**
   * The helpdesk integration services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * Service constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk integration services.
   */
  public function __construct(BaseService $service) {
    $this->service = $service;
  }

  /**
   * Get a list of active Uvdesk instances.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   A list of active Uvdesk instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getUvdeskInstances(): array {
    $instances = [];
    foreach ($this->service->getHelpdeskInstances() as $helpdeskInstance) {
      $plugin = $helpdeskInstance->getPlugin();
      if ($plugin instanceof Uvdesk) {
        $instances[] = $helpdeskInstance;
      }
    }
    return $instances;
  }

}
